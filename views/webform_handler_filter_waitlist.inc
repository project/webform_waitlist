<?php

/**
 * @file
 * Contains \webform_handler_filter_waitlist.
 */

/**
 * Handler to filter webforms by waitlist state.
 */
class webform_handler_filter_waitlist extends views_handler_filter_in_operator {

  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Status');
      $options = array('0' => t('Disabled'), '1' => t('Enabled'));
      $this->value_options = $options;
    }
  }

  // '0' won't work as a key for checkboxes.
  // @todo Is above comment a D6 issue?
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $form['value']['#type'] = 'select';
  }
}
