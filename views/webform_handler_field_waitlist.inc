<?php

/**
 * @file
 * Contains \webform_handler_field_waitlist.
 */

/**
 * Field handler to display the waitlist status of a webform.
 *
 * @ingroup views_field_handlers
 */
class webform_handler_field_waitlist extends views_handler_field {

  function render($values) {
    $is_waitlist = $values->{$this->field_alias};
    return isset($waitlist)
              ? ($waitlist ? t('enabled') : t('disabled'))
              : t('no webform');
  }
}
