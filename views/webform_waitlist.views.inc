<?php

/**
 * @file
 * Provides views hook implementations.
 */

/**
 * Implements hook_views_data_alter().
 */
function webform_waitlist_views_data_alter(&$data) {
  // Is wait-list enabled for the webform.
  $data['webform']['waitlist'] = array(
    'title' => t('Wait-list'),
    'help' => t('Whether a wait-list is enabled on the webform.'),
    'field' => array(
      'handler' => 'webform_handler_field_waitlist',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'webform_handler_filter_waitlist',
    ),
    'sort' => array(
       'handler' => 'views_handler_sort',
    ),
  );

  // Is submission on a wait-list.
  $data['webform_submissions']['is_waitlist'] = array(
    'title' => t('Wait-list'),
    'help' => t('Whether the submission is on a wait-list.'),
    'field' => array(
      'handler' => 'webform_handler_field_is_waitlist',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'webform_handler_filter_is_waitlist',
    ),
    'sort' => array(
       'handler' => 'views_handler_sort',
    ),
  );
}
