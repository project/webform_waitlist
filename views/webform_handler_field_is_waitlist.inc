<?php

/**
 * @file
 * Contains \webform_handler_field_is_waitlist.
 */

/**
 * Field handler to display the waitlist status of a submission.
 *
 * @ingroup views_field_handlers
 */
class webform_handler_field_is_waitlist extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();

    // 'Seated' in analogy to a restaurant or entertainment venue.
    $options += array(
      'waitlist' => array('default' => 'waitlist'),
      'seated' => array('default' => 'seated'),
    );

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['waitlist_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Waitlist settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['waitlist'] = array(
      '#type' => 'textfield',
      '#title' => t('On waitlist'),
      '#description' => t('Display text if submission is on waitlist'),
      '#default_value' => $this->options['waitlist'],
      '#fieldset' => 'waitlist_settings',
    );
    $form['seated'] = array(
      '#type' => 'textfield',
      '#title' => t('Not on waitlist'),
      '#description' => t('Display text if submission is NOT on waitlist'),
      '#default_value' => $this->options['seated'],
      '#fieldset' => 'waitlist_settings',
    );
  }

  function render($values) {
    $is_waitlist = $values->{$this->field_alias};
    return isset($is_waitlist)
              ? ($is_waitlist ? t($this->options['waitlist']) : t($this->options['seated']))
              : t('no submission');
  }
}
