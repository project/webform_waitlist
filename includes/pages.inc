<?php

/**
 * @file
 * Provides form routines for processing a webform submission.
 */

/**
 * Sets waitlist flag on node object.
 *
 * @param object $node
 *   The node object.
 */
function webform_waitlist_node_flag_set(&$node) {
  if ($node->webform['waitlist']) {
    // Waiting list is enabled.
    $count = webform_get_submission_count($node->nid);
    $node->is_waitlist = $count >= $node->webform['waitlist_threshold'];
  }
  else {
    $node->is_waitlist = FALSE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for webform_client_form.
 *
 * @todo Requires patch to webform to add our column to submission load.
 */
function _webform_waitlist_form_webform_client_form_alter(&$form, &$form_state, $form_id) {
  // Gather information.
  $node = &$form['#node'];
  $submission = $form['#submission'];
  $menu_item = menu_get_item();
  $form_state['is_admin_user'] = $menu_item['path'] == 'node/%/submission/%/edit';

  // Include this file during form processing.
  form_load_include($form_state, 'inc', 'webform_waitlist', 'includes/pages');

  // Add the submission handler to the top of the list.
  array_unshift($form['#submit'], 'webform_waitlist_client_form_submit');

  if ($form_state['is_admin_user']) {
    // Admin user is editing the submission.
    if ($node->webform['waitlist'] && $form_state['webform']['page_num'] == 1) {
      // Waiting list is enabled on this form.
      if (empty($form_state['input']) || $form_state['submitted']) {
        // Conditions apply as follows:
        // - first when form is first displayed
        // - second when form is rebuilt during form processing workflow
        //
        // Display waitlist checkbox.
        $form['submitted']['is_waitlist'] = array(
          '#type' => 'checkbox',
          '#title' => t('On waiting list'),
          '#description' => t('Is this submission on a waiting list?'),
          '#default_value' => isset($form_state['storage']['is_waitlist']) ? $form_state['storage']['is_waitlist'] : $submission->is_waitlist,
          '#weight' => -10,
        );
        // Display message to inform admin user that changes to waitlist require
        // manual notification to the submission user.
        $message = t('<strong>A wait-list is in effect for this form.</strong> If you change the wait-list status of this submission, then you will need to notify the submission user.');
        $type = 'warning';
        // @todo Do something more exotic???
        drupal_set_message($message, $type, FALSE);
      }
    }
  }
  else {
    // Regular user is making a submission.
    // If waitlist_threshold is exceeded then display a message to inform user
    // of being on the waitlist.
    webform_waitlist_node_flag_set($node);
    if ($node->is_waitlist) {
      $message = $node->webform['waitlist_notice'];
      $type = 'warning';
      // @todo Do something more exotic???
      drupal_set_message($message, $type, FALSE);
    }
  }
}

/**
 * Form submission handler for webform_client_form().
 *
 * This is called before webform_client_form_submit().
 */
function webform_waitlist_client_form_submit($form, &$form_state) {
  $node = &$form['#node'];

  if ($form_state['is_admin_user']) {
    // Admin user is editing the submission.
    if ($node->webform['waitlist'] && $form_state['webform']['page_num'] == 1) {
      // Store value from page one so it persists through webform pages.
      // The 'is_waitlist' checkbox only displays on page one.
      // If unchecked, then it is omitted from input and values arrays due to
      // processing in webform module.
      $form_state['storage']['is_waitlist'] = isset($form_state['input']['submitted']['is_waitlist']) ? $form_state['input']['submitted']['is_waitlist'] : 0;
    }
    // Set waitlist flag on node object to persist through submission workflow.
    // Do always as webform may only have one page and webform state flags
    // ('save_draft', 'webform_completed') are not yet set.
    $node->is_waitlist = $form_state['storage']['is_waitlist'];
  }
  else {
    // Regular user is making a submission.
    // Set waitlist flag on node object to persist through submission workflow.
    webform_waitlist_node_flag_set($node);
  }
}

/**
 * Implements hook_webform_submission_presave().
 *
 * This is called during the webform_client_form_submit() workflow either from
 * webform_submission_insert() or webform_submission_update().
 */
function webform_waitlist_webform_submission_presave($node, $submission) {
  $submission->is_waitlist = $node->is_waitlist;
}
