<?php

/**
 * @file
 * Provides form routines for configuring a webform.
 */

/**
 * Implements hook_form_FORM_ID_alter() for webform_configure_form.
 */
function _webform_waitlist_form_webform_configure_form_alter(&$form, &$form_state, $form_id) {
  // Gather information.
  $node = $form['#node'];
  // @todo State code fails to hide the text format and the token values fieldset
  // which are built as part of element processing.
  $states = array(
    'invisible' => array(
      ':input[name="waitlist"]' => array('checked' => FALSE),
    ),
  );

  // Include this file during form processing.
  form_load_include($form_state, 'inc', 'webform_waitlist', 'includes/admin');

  // Add the submission handler to the top of the list.
  array_unshift($form['#submit'], 'webform_waitlist_configure_form_submit');

  // Add form elements.
  $form['waitlist'] = array(
    '#type' => 'fieldset',
    '#title' => t('Wait-list settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE, // TEMP to test
    '#weight' => 11, // TEMP to test
  );

  $form['waitlist']['waitlist'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable a waiting list'),
    '#description' => t('Impose a waiting list once the total number of submissions reaches the threshold value.'),
    '#default_value' => $node->webform['waitlist'],
    '#id' => 'webform-waitlist-enable',
  );
  // @todo Enforce positive if above checkbox is checked.
  // Is there a form element that does this? HTML5 one?
  $form['waitlist']['waitlist_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Wait-list threshold'),
    '#maxlength' => 8,
    '#size' => 8,
    '#description' => t('The threshold number of submissions before a waiting list begins.'),
    '#default_value' => $node->webform['waitlist_threshold'] > 0 ? $node->webform['waitlist_threshold'] : '',
//     '#parents' => array('waitlist_threshold'),
    '#states' => $states,
    '#element_validate' => array('webform_waitlist_configure_form_validate'),
  );
  // @todo States JS does not get applied to filter format fieldset nor the
  // description. Can we easily make it so?
  $form['waitlist']['waitlist_notice'] = array(
    '#type' => 'text_format',
    '#title' => t('Wait-list notification message'),
    '#description' => t('Message to be shown upon webform display after waitlist is in effect. Supports Webform token replacements.'),
    '#default_value' => $node->webform['waitlist_notice'],
    '#cols' => 40,
    '#rows' => 10,
    '#format' => $node->webform['waitlist_notice_format'],
    '#parents' => array('waitlist_notice'),
    '#states' => $states,
  );
  // @todo Separating this from above element improves the CSS as being part of
  // above causes CSS bleed on the fieldset legend.
  $form['waitlist']['token3'] = array(
    '#type' => 'container',
    '#theme' => 'webform_token_help',
    '#groups' => array('node', 'waitlist'),
//     '#parents' => array('waitlist_notice'),
    '#states' => $states,
  );
}

/**
 * Element validation handler for webform_configure_form().
 */
function webform_waitlist_configure_form_validate($element, &$form_state, $form) {
  // @todo Validate the waitlist_threshold being a positive integer.
}

/**
 * Form submission handler for webform_configure_form().
 */
function webform_waitlist_configure_form_submit($form, &$form_state) {
  $webform = &$form['#node']->webform;
  $values = $form_state['values'];

  // Save the configuration.
  $webform['waitlist'] = (int) $values['waitlist'];
  $webform['waitlist_threshold'] = !$values['waitlist'] ? 0 : (int) $values['waitlist_threshold'];
  // Retain these values in case user toggles the waitlist checkbox.
  $webform['waitlist_notice'] = $values['waitlist_notice']['value'];
  $webform['waitlist_notice_format'] = $values['waitlist_notice']['format'];
}
