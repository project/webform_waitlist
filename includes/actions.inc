<?php

/**
 * @file
 * Implements actions for use with views bulk operations.
 */

/**
 * Implements hook_action_info().
 */
function _webform_waitlist_action_info() {
  // @todo If type of 'entity' is used with its sets_property behavior would
  // we need to write anything?
  return array(
    'webform_waitlist_waitlist_set' => array(
      'type' => 'webform_submissions',
      'label' => t('Add to wait-list'),
      'configurable' => FALSE,
      'behavior' => array('sets_property'),
      'triggers' => array('any'),
      'aggregate' => TRUE,
    ),
    'webform_waitlist_waitlist_unset' => array(
      'type' => 'webform_submissions',
      'label' => t('Remove from wait-list'),
      'configurable' => FALSE,
      'behavior' => array('sets_property'),
      'triggers' => array('any'),
      'aggregate' => TRUE,
    ),
  );
}

/**
 * Action callback: sets is_waitlist to TRUE.
 *
 * @todo User needs to design the view to restrict the list to webform nodes
 * that have wait-list enabled.
 */
function webform_waitlist_waitlist_set($entities, $context) {
  $ids = array_keys($entities);
  webform_waitlist_waitlist_update($ids, 1);
}

function webform_waitlist_waitlist_unset($entities, $context) {
  $ids = array_keys($entities);
  webform_waitlist_waitlist_update($ids, 0);
}

function webform_waitlist_waitlist_update($ids = array(), $value = 1) {
  sort($ids);
  db_update('webform_submissions')
    ->fields(array(
      'is_waitlist' => $value,
    ))
    ->condition('sid', $ids, 'IN')
    ->execute();
}
