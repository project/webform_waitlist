<?php

/**
 * @file
 * Provides info-style hooks that are infrequently called.
 */

/**
 * Implements hook_entity_info().
 *
 * This is done to simplify the use of Views Bulk Operations which automatically
 * provides support for all entities. But this opens up many other possibilities
 * for Webform.
 *
 * @todo Patch webform module to provide this.
 */
function _webform_waitlist_entity_info() {
  // Similar to user which has no bundle per se.
  $info = array(
    'webform_submissions' => array(
      'label' => t('WebformSubmissions'),
      'controller class' => 'WebformSubmissionsController',
      'base table' => 'webform_submissions',
      'uri callback' => 'webform_submissions_uri',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'sid',
      ),
      'bundles' => array(
        'webform_submissions' => array(
          'label' => t('Webform Submissions'),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Webform Submissions'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
  return $info;
}
